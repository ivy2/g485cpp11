#include <cstddef>

#include <vector>
#include <iostream>
#include <map>
#include <memory>
#include <limits>

static int count = 0;

using namespace std;

template <typename T>
class MyAlloc 
{
public:
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;
  typedef T* pointer;
  typedef const T* const_pointer;
  typedef T& reference;
  typedef const T& const_reference;
  typedef T value_type;

  MyAlloc() throw() {}
  MyAlloc(const MyAlloc&) throw() {}

  template <typename U>
  MyAlloc (const MyAlloc<U>&) throw() {}
  ~MyAlloc() throw() {}

  T* allocate (std::size_t num, const void* hint = 0) 
  {
    count += num*sizeof(T);
    cout << "In allocate: count = " << count << endl;
    return static_cast<T*>(::operator new(num*sizeof(T)));
  }

  void deallocate (T* p, std::size_t num) 
  {
    count -= num*sizeof(T);
    cout << "In deallocate: count = " << count << endl;
    ::operator delete(p);
  }
  
  T* address (T& value) const 
  {
    return &value;
  }
  
  const T* address (const T& value) const 
  {
    return &value;
  }

  std::size_t max_size () const throw() 
  {
    return std::numeric_limits<std::size_t>::max() / sizeof(T);
  }

  void construct (T* p, const T& value) 
  {
    ::new((void*)p)T(value);
  }

  void destroy (T* p) 
  {
    p->~T();
  }

  template <typename U>
  struct rebind {
    typedef MyAlloc<U> other;
  };
};

template <typename T1, typename T2>
bool operator== (const MyAlloc<T1>&,
		 const MyAlloc<T2>&) noexcept {
  return true;
}
template <typename T1, typename T2>
bool operator!= (const MyAlloc<T1>&,
		 const MyAlloc<T2>&) noexcept {
  return false;
}


template <typename T>
using STLVector = vector<T, MyAlloc<T>>;


template<class K, class D>
using STLMap = map<K,D, less<K>, MyAlloc<pair<K,D>>>;


