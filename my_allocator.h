#include <cstddef>

#include <vector>
#include <iostream>
#include <map>

static int count = 0;

using namespace std;

template <typename T>
class MyAlloc {
public:
  typedef T value_type;

  MyAlloc () noexcept {
  }

  template <typename U>
  MyAlloc (const MyAlloc<U>&) noexcept {

  }

  T* allocate (std::size_t num) {
    count += num*sizeof(T);
    cout << "In allocate: count = " << count << endl;
    return static_cast<T*>(::operator new(num*sizeof(T)));
  }

  void deallocate (T* p, std::size_t num) {
    count -= num*sizeof(T);
    cout << "In deallocate: count = " << count << endl;    
    ::operator delete(p);
  }
};

template <typename T1, typename T2>
bool operator== (const MyAlloc<T1>&,
		 const MyAlloc<T2>&) noexcept {
  return true;
}
template <typename T1, typename T2>
bool operator!= (const MyAlloc<T1>&,
		 const MyAlloc<T2>&) noexcept {
  return false;
}


template <typename T>
using STLVector = vector<T, MyAlloc<T>>;


template<class K, class D>
using STLMap = map<K,D, less<K>, MyAlloc<pair<K,D>>>;


