CXXFLAGS=-std=c++11

works:	works.C my_allocator.h

works_98:	works_98.C my_allocator_98.h

not:	not.C my_allocator.h

not_98:	not_98.C my_allocator_98.h

clean:	
	rm -f works not works_98 not_98 *~ *.o
