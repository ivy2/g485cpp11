# README #

### What is this repository for? ###

* Does gcc 4.8.5 really fully support C++11 [ as claimed ](https://gcc.gnu.org/projects/cxx-status.html#cxx11) ?
* works.C compiles and runs for gcc 4.8.5 and gcc 6.2
* not.C works for gcc 6.2 but does not compile for gcc 4.8.5. 
* not.C does not follow C++11 standard or gcc 4.8.5 does not fully support C++11?
* The allocator in my_allocator.h is defined according to C++11 syntax as described on page 1025 of "The C++ Standard Library A Tutorial and Reference", Second Edition, by Nicolai M. Josuttis
* The allocator in my_allocator_98.h is defined according to C++98 syntax as described on page 1150 of "The C++ Standard Library A Tutorial and Reference", Second Edition, by Nicolai M. Josuttis

* CXXFLAGS=-std=c++11
* not_98.C and works_98.C work with both gcc 4.8.5 and gcc 6.2
* not.C does not work with intel/16.0 but not_98.C works; works.C and works_98.C work fine with intel/16.0
* not_98.C and works_98.C both work fine with PGI


